package com.example.qesja.probeersealarmclock;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity{
    public Context context;
    List<String> alarms;
    ListView listAlarm;
    ArrayAdapter<String> adapter;
    AlarmManager alarm_manager;
    PendingIntent pending_intent;
    private SharedPreferences preferences = null;

    private static final DateFormat formatDateTime =
            new SimpleDateFormat("HH:mm");

    Calendar dateTime = Calendar.getInstance();
    private TextView text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        context = this;
        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        //Initialize timemanager

        alarm_manager = (AlarmManager) getSystemService(ALARM_SERVICE);

        //alarms
        alarms = new ArrayList<String>();


        if (preferences.getBoolean(getString(R.string.pref_needs_setup), true)) {
            startSettingsActivity();
        }



        adapter = new ArrayAdapter<String>(context,android.R.layout.simple_list_item_1,alarms);
        listAlarm  = (ListView) findViewById(R.id.listAlarm);
        listAlarm.setAdapter(adapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateTime();
            }
        });







    }


    private void startSettingsActivity() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    private void updateTime() {
        new TimePickerDialog(this, t, dateTime.get(Calendar.HOUR_OF_DAY), dateTime.get(Calendar.MINUTE), true).show();

    }

    TimePickerDialog.OnTimeSetListener t = new TimePickerDialog.OnTimeSetListener() {



        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            dateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
            dateTime.set(Calendar.MINUTE, minute);
            addAlarm();



        }


    };

    private void addAlarm(){

        ArrayList<PendingIntent> intentArray = new ArrayList<PendingIntent>();

        alarms.add(formatDateTime.format(dateTime.getTime()));
        adapter.notifyDataSetChanged();
        long sdl = dateTime.getTimeInMillis();

        final Intent intent = new Intent(MainActivity.this, Alarm_Receiver.class);


        final int _id = (int) System.currentTimeMillis();
        final PendingIntent appIntent = PendingIntent.getBroadcast(this, _id, intent,PendingIntent.FLAG_ONE_SHOT);

        alarm_manager.setRepeating(AlarmManager.RTC_WAKEUP, sdl, AlarmManager.INTERVAL_DAY,appIntent);

        intentArray.add(appIntent);



        //Remove alarm on long press
        listAlarm.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long arg3) {
                Log.e("removed","");

                adapter.remove(alarms.get(position));
                adapter.notifyDataSetChanged();


                alarm_manager.cancel(appIntent);


                CharSequence text = "Alarm deleted!";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

                return false;


            }

        });
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent();

            i.setClassName("com.example.qesja.probeersealarmclock" , "com.example.qesja.probeersealarmclock.SettingsActivity");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



}
