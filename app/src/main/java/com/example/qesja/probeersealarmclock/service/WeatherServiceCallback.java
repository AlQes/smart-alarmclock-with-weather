package com.example.qesja.probeersealarmclock.service;

import com.example.qesja.probeersealarmclock.data.Channel;

/**
 * Created by Aldo on 23/05/2017.
 */

public interface WeatherServiceCallback {
    void serviceSucces(Channel channel);
    void serviceFailure(Exception exception);
}
