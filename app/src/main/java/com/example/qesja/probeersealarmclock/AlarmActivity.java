package com.example.qesja.probeersealarmclock;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.qesja.probeersealarmclock.data.Channel;
import com.example.qesja.probeersealarmclock.data.Condition;
import com.example.qesja.probeersealarmclock.data.Item;
import com.example.qesja.probeersealarmclock.data.Units;
import com.example.qesja.probeersealarmclock.service.WeatherServiceCallback;
import com.example.qesja.probeersealarmclock.service.YahooWeatherService;

public class AlarmActivity extends AppCompatActivity implements WeatherServiceCallback{

    MediaPlayer media_song;
    private ImageView weatherIconImageView;
    private TextView temperatureTextView;
    private TextView locationTextView;
    private TextView conditionTextView;


    private YahooWeatherService service;
    private ProgressDialog dialog;
    private SharedPreferences preferences = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);
        String location = null;

        weatherIconImageView = (ImageView)findViewById(R.id.weatherIconImageView);
        temperatureTextView = (TextView)findViewById(R.id.temperatureTextView);
        conditionTextView = (TextView)findViewById(R.id.conditionTextView);
        locationTextView = (TextView)findViewById(R.id.locationTextView);


        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        service = new YahooWeatherService(this);
        dialog = new ProgressDialog(this);
        dialog.setMessage("loading ...");


        location = preferences.getString(getString(R.string.pref_manual_location), null);

        service.refreshWeather(location);

        media_song = MediaPlayer.create(this, R.raw.iphone7);
        media_song.setLooping(true);

        media_song.start();
        Button alarm_off = (Button) findViewById(R.id.offButton);
        //Stop ringtoneservice
        alarm_off.setOnClickListener((v) -> {
                media_song.stop();

            finish();
        });





        }


    @Override
    public void serviceSucces(Channel channel) {
        dialog.hide();

        Item item = channel.getItem();
        Condition condition = channel.getItem().getCondition();
        Units units = channel.getUnits();
        int resourceId = getResources().getIdentifier("drawable/icon_" + item.getCondition().getCode(), null, getPackageName());

        @SuppressWarnings("deprecation")
        Drawable weatherIconDrawable = getResources().getDrawable(resourceId);

        weatherIconImageView.setImageDrawable(weatherIconDrawable);

        temperatureTextView.setText(condition.getTemperature() +"\u00B0"+ units.getTemperature());
        conditionTextView.setText(item.getCondition().getDescription());
        locationTextView.setText(service.getLocation());
    }

    @Override
    public void serviceFailure(Exception exception) {
        dialog.hide();
        Toast.makeText(this, exception.getMessage(), Toast.LENGTH_LONG).show();
    }
}

