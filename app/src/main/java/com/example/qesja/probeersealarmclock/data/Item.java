package com.example.qesja.probeersealarmclock.data;

import org.json.JSONObject;

/**
 * Created by Aldo on 23/05/2017.
 */

public class Item implements JSONPopulator {
    private Condition condition;

    public Condition getCondition() {
        return condition;
    }

    @Override
    public void populate(JSONObject data) {
        condition = new Condition();
        condition.populate(data.optJSONObject("condition"));
    }
}
