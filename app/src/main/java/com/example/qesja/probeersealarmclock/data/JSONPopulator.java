package com.example.qesja.probeersealarmclock.data;

import org.json.JSONObject;

/**
 * Created by Aldo on 23/05/2017.
 */

public interface JSONPopulator {

    void populate(JSONObject data);
}
