package com.example.qesja.probeersealarmclock.data;

import org.json.JSONObject;

/**
 * Created by Aldo on 23/05/2017.
 */

public class Units implements JSONPopulator {
    private String temperature;

    public String getTemperature() {
        return temperature;
    }

    @Override
    public void populate(JSONObject data) {
        temperature = data.optString("temperature");
    }
}
